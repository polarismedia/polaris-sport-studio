var logging = true;
var liveUrl = "http://live.adressa.no/i/";
var liveBaseUrl = "http://live.adressa.no";
var liveStudioIframeUrl = "http://mm.aftenposten.no/tools/livestudio_v2/livestudio_iframe/livestudio_iframe.html?livestudio_id=927";
var leagueUrl = "http://live.adressa.no/data/mobile/table/?id=706&callback=foobar";
var matchesUrl = "http://www.adressa.no/template/custom/code/100sport/matches.jsp";

function getCharsBefore(str, chr) {
    var index = str.indexOf(chr);
    if (index != -1) {
        return(str.substring(0, index));
    }
    return("");
}
function getParameters(location) {
    if (typeof location === 'undefined') {
        location = window.location;
    }
    var hashParams = new (function Params() {
    })();
    if (location.hash.length === 0) {
        return hashParams;
    }
    ;
    var hashArray = location.hash.substring(1).split('&');
    for (var i in hashArray) {
        var keyValPair = hashArray[i].split('=');
        hashParams[keyValPair[0]] = keyValPair[1];
    }
    return hashParams;
}

function calculateIframeHeight() {
    var iframeHeight = $('#iframeDiv').parent().height();
    $('#iframeDiv').height(iframeHeight);
    $('#iframeDiv iframe').height(iframeHeight+82);    
}

function updateIframe() {
    var hashValues = getParameters(window.location);
    var matchId = hashValues['matchId'];
    var liveStudioId = hashValues['liveStudioId'];
    if (liveStudioId !== undefined) {
        var currentSrc = $("#liveStudioFrame").attr("src");
        if (currentSrc === undefined) {
            currentSrc = liveStudioIframeUrl;
        }
        var baseUrl = currentSrc.substring(0, currentSrc.indexOf("?"));
        var newSrc = baseUrl + "?livestudio_id=" + liveStudioId;
        $("#liveStudioFrame").attr("src", newSrc);
        if (logging) { 
            console.log("Updating to liveStudioId: " + liveStudioId);
        }
    }
    if (matchId !== undefined) {
        var newMatchSrc = liveUrl + "#" + matchId;
        if ($("#liveIframe").length > 0) {
            $("#liveIframe").remove();
        } 
        $("#iframeDiv").append("<iframe id='liveIframe' border='0'></iframe>");
        $("#liveIframe").attr("src", newMatchSrc);
        if (logging) { 
            console.log("Updating to matchID: " + matchId);
        }
    }
    $("#liveIframe").on("load",function(){
        calculateIframeHeight();
    });
}

function updateLeagueTable(data) {
    $("#leagueTable").find("caption").html("Sist oppdatert: " + data[1]);
    $("#leagueTable tbody").empty();
    for (i = 4; i < data.length; i++) {
        if (data[i][0]=='groupHeader') {
            var groupRow = $("<tr><td colspan='8'><strong>" + data[i][1]  + "</strong></td></tr>");
            $("#leagueTable").find("tbody").append(groupRow);        
        } else {
            var rowdata = data[i];
            var tableRow = $("<tr class='even'></tr>");
            var tdPostion = $("<td>" + rowdata[2] + "</td>");
            var tdTeam = $("<td>" + rowdata[3] + "</td>");
            var tdPlayed = $("<td>" + rowdata[4] + "</td>");
            var tdWon = $("<td>" + rowdata[5] + "</td>");
            var tdTie = $("<td>" + rowdata[6] + "</td>");
            var tdLost = $("<td>" + rowdata[7] + "</td>");
            var tdGoals = $("<td>" + rowdata[8] + "</td>");
            var tdPoints = $("<td>" + rowdata[9] + "</td>");
            $(tableRow).append(tdPostion);
            $(tableRow).append(tdTeam);
            $(tableRow).append(tdPlayed);
            $(tableRow).append(tdWon);
            $(tableRow).append(tdTie);
            $(tableRow).append(tdLost);
            $(tableRow).append(tdGoals);
            $(tableRow).append(tdPoints);
            $("#leagueTable").find("tbody").append(tableRow);
        }    
    }
}

function updateLeagueData(url) {
    console.log(url);
    $.ajax({
        url: url,
        jsonp: "callback",
        dataType: "jsonp",
        success: function (response) {
            updateLeagueTable(response);            
        }
    });    
}

function updateTodaysMatches() {
    $("#hidden_kamper").empty();
    $("#kamper").empty();
    $.ajax({
        url: matchesUrl,
        dataType: 'html',
        success: function(response) {
            $("#hidden_kamper").append(response);
            $("#kamper").append($("#hidden_kamper").find(".header"));
            $(".norwegianActions").parent().empty();
            $("#kamper .header a").each(function(index,value){
                var url = $(value).attr("href");
                url = url.substring(1,url.length);
                $(value).attr("href",liveBaseUrl + "#frontpage=" + url);
            });
            $("#hidden_kamper").find(".leagueWrap").each(function (index, value) {
                if (index == 0) {
                    $("#kamper").append(value);
                } else {
                    $(value).find(".showTextLive").each(function(i,obj){
                        var href = $(obj).attr("href");
                        if (href !== undefined) {
                            href = href.substring(1, href.length)
                        }                         
                        $(obj).attr("href", "#matchId=" + href);
                        $(obj).html("Hendelser");
                        /* Little hack on livestudio template to make it activate the top right panel */
                        $(obj).click(function(){
                           if ($(window).width() < 800) {
                                $(".mobile-menu-content li").hide();
                                $('.mobile-menu-content li[data-name="box-element-3"]').show();
                               var _activeTabName = "box-element-3";
                               var _tabName = "Kampforl&oslash;p";                            
                               $('.mobile-menu ul li a').html(_tabName);
                               $('section[data-name="box-right-bottom"] section[data-name="box-right-content"] > div').hide();
                               $('div[data-name="' + _activeTabName + '"]').show();
                               $('div[data-name="' + _activeTabName + '"]').css("padding:0");
                           }
                        });
                    });
                    $("#kamper").append(value);                    
                }
            });
        }
    });    
    setTimeout(updateTodaysMatches,60000);
}

$(document).ready(function () {
    //OPPDATERING HASH
    updateIframe();
    $(window).bind('hashchange', function (e) {
        updateIframe();
    });
    //LIGA TABELLER
    console.log(leagueUrl);
    updateLeagueData(leagueUrl);
    $("#leagueSwitcher").change(function () {
        var leagueId = $(this).val();
        var newLeagueUrl = getCharsBefore(leagueUrl,'?') + "?id=" + leagueId + "&callback=foobar";
        updateLeagueData(newLeagueUrl);       
        if (logging) { 
            console.log("Switching to leagueId: " + leagueId);
            console.log("...on "  + newLeagueUrl);
        }
    });    
    //DAGENS KAMPER
    updateTodaysMatches();
});